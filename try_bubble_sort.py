def bubble_sort(data: list) -> list:
    sorted_data = data.copy()

    for i in range(len(data)):
        for j in range(len(data)):
            if sorted_data[i] < sorted_data[j]:
                sorted_data[i], sorted_data[j] = sorted_data[j], sorted_data[i]

    return sorted_data


if __name__ == "__main__":
    data = list(map(int, input("Enter 10 number: ").split()))

    sorted_data = bubble_sort(data)

    print(sorted_data)
